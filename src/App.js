import { Switch, Route, Redirect } from "react-router-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Home from "./Components/Home";
import About from "./Components/About";
import Contact from "./Components/Contact";
import Service from "./Components/Service";
import Navbar from "./Components/Navbar";
import Common from "./Components/Common";
import Footer from "./Components/Footer";


function App() {
  return (
    <div className="App">
      <Navbar />
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/About" component={About}/>
          <Route exact path="/Contact" component={Contact}/>
          <Route exact path="/Service" component={Service}/>
          <Redirect to="/" /> 
        </Switch>
      <Footer />
    </div>
  );
}

export default App;
