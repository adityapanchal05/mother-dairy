const Footer = () => {
    return(
        <>
        <footer className="w-100 bg-light text-center">
            <p>© 2021 KP Enterprise. All Rights Reserved | Terms and Conditions</p>
        </footer>
        </>
    )
}

export default Footer