import React from "react-router-dom";
import Common from "./Common";
import web from "./Images/About.svg";


const About = () => {
    return (
        <>
        <Common name="Welcome To About Page" imgsrc={web} visit="/Contact" btname="Contact Now"/>
        </>
    );
};

export default About;
