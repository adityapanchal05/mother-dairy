import React, { NavLink } from "react-router-dom";
import Common from "./Common";
import web from "./Images/home.svg";

const Home = () => {
    return (
        <>
        <Common name="Grow your business with" imgsrc={web} visit="/Service" btname="Get Started"/>
        </>
    );
};

export default Home;
