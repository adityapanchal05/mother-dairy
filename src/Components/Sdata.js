import Web from "./Images/img2.jpg";
import App from "./Images/img3.jpg";
import Software from "./Images/img4.jpg";
import Digital from "./Images/img5.jpg";
import Android from "./Images/img6.jpg";
import Marketing from "./Images/img7.jpg";

const Sdata = [
    {
        imgsrc: Web,
        title: "Web Development",
    },
    {
        imgsrc: App,
        title: "App Development",
    },
    {
        imgsrc: Software,
        title: "Software Development",
    },
    {
        imgsrc: Digital,
        title: "Digital Development",
    },
    {
        imgsrc: Android,
        title: "Android Development",
    },
    {
        imgsrc: Marketing,
        title: "Marketing Development",
    },
];

export default Sdata;
